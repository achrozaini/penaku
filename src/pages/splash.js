import React, {useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import Icon from '../assets/icon.png';

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Home');
        }, 3000);
    });
    return (
        <View style={styles.wrapper}>
            <Image source={Icon} style={styles.icon} />
        </View>
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        margin: 10,
        width: 90,
        height: 90,
    },
    loadText: {
        fontSize: 18,
        fontWeight: 'bold',
    }
});
