import React, { Component } from 'react'
import { Text, StyleSheet, View, TouchableOpacity, ScrollView, Alert } from 'react-native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import FIREBASE from '../database'
import { Tampil } from '../components'

export default class Home extends Component {
    constructor(props) {
        super(props)

        this.state = {
            daftarCatatan: {},
            daftarCatatanKey: [],
        }
    }

    componentDidMount() {
        this.ambilData();
    }

    ambilData = () => {
        FIREBASE.database()
            .ref("Catatan")
            .once('value', (querySnapshot) => {
                let data = querySnapshot.val() ? querySnapshot.val() : {}
                let catatanItem = {...data}

                this.setState({
                    daftarCatatan: catatanItem,
                    daftarCatatanKey: Object.keys(catatanItem)
                })
            })
    }

    removeData = (id) => {
        Alert.alert(
            "INFO",
            "Apakah Anda Akan Menghapus Catatan Ini?",
            [
                {
                    text: "Cancel",
                    onPress: () => console.log("OOOOkay"),
                    style: "cancel"
                },
                { text: "OK", onPress:() => {
                    FIREBASE.database()
                    .ref('Catatan/'+id)
                    .remove();
                    this.ambilData();
                    
                    Alert.alert('Hapus', 'Sukses Hapus Catatan')
                } }
            ],
            { cancelable: false }
        );
    }

    render() {
        const { daftarCatatan, daftarCatatanKey } = this.state
        return (
            <View style={styles.page}>
                <View style={styles.header}>
                    <Text style={styles.home}>Home</Text>
                </View>

                <ScrollView style={styles.kotakCat}>
                    {daftarCatatanKey.length > 0 ? (
                        daftarCatatanKey.map((key) => (
                            <Tampil key={key} catatanItem={daftarCatatan[key]} id={key} {...this.props} removeData={this.removeData} />
                        ))
                    ) : (
                        <Text>Tidak ada catatan</Text>
                    )}
                </ScrollView>

                <View style={styles.kotak}>
                    <TouchableOpacity style={styles.tambah} onPress={() => this.props.navigation.navigate('TulisCatatan')}>
                        <FontAwesomeIcon icon={faPlus} size={15} color={'white'} />
                    </TouchableOpacity>
                </View>

                <View style={styles.catatan}>
                    <Text style={styles.tbhCatatan}>Tulis Catatan Anda</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white',
    },
    header: {
        backgroundColor: 'orange',
        padding: 15,
    },
    home: {
        fontSize: 24,
        fontWeight: 'bold',
        color: 'white',
    },
    kotak: {
        flex: 1,
        position: 'absolute',
        top: 75,
        left: 15,
    },
    tambah: {
        padding: 20,
        backgroundColor: 'black',
        borderRadius: 30,
    },
    catatan: {
        flex: 1,
        position: 'absolute',
        top: 92,
        left: 80,
    },
    tbhCatatan: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    kotakCat: {
        paddingTop: 80,
        paddingHorizontal: 20,

    }
})
