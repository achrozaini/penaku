import React, { Component } from 'react'
import { Text, StyleSheet, View } from 'react-native'
import FIREBASE from '../database'

export default class Detail extends Component {
    constructor(props) {
        super(props)
    
        this.state = {
             daftarCatatan: {}
        }
    }

    componentDidMount() {
        FIREBASE.database()
            .ref('Catatan/' + this.props.route.params.id)
            .once('value', (querySnapShot) => {
                let data = querySnapShot.val() ? querySnapShot.val() : {};
                let catatanItem = {...data};

                this.setState({
                    daftarCatatan: catatanItem,
                })
            })
    }
    
    render() {
        const {daftarCatatan} = this.state;
        return (
            <View style={styles.page}>
                <View style={styles.kotak}>
                    <Text style={styles.judul}>{daftarCatatan.judul}</Text>
                    <View style={styles.line}></View>
                    <Text style={styles.catatan}>{daftarCatatan.catatan}</Text>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white',
    },
    kotak: {
        padding: 20,
    },
    judul: {
        fontSize: 24,
        fontWeight: 'bold',
        paddingLeft: 10,
        color: 'black',
    },
    catatan: {
        fontSize: 18,
        padding: 10,
        color: 'black',
        marginBottom: 18,
        textAlignVertical: 'top',
    },
    line: {
        borderWidth: 1,
        marginTop: 10,
    }
});
