import Home from './home'
import TulisCatatan from './tuliscatatan'
import Detail from './detail'
import EditCatatan from './editcatatan'
import Splash from './splash'

export { Home, TulisCatatan, Detail, EditCatatan, Splash }