import React, { Component } from 'react'
import { StyleSheet, View, TouchableOpacity, Alert } from 'react-native'
import { InputTeks } from '../components'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faCheck } from '@fortawesome/free-solid-svg-icons'
import FIREBASE from '../database'

export default class TulisCatatan extends Component {
    
    constructor(props) {
        super(props)

        this.state = {
            judul: '',
            catatan: '',
        }
    }

    onChangeText = (namaState, value) => {
        this.setState({
            [namaState]: value
        })
    }

    onSubmit = () => {
        if (this.state.judul && this.state.catatan) {
            const catatanReferensi = FIREBASE.database().ref('Catatan')
            const catatan = {
                judul: this.state.judul,
                catatan: this.state.catatan
            }

            catatanReferensi
                .push(catatan)
                .then((data) => {
                    Alert.alert('Sukses','Catatan tersimpan')
                    this.props.navigation.replace('Home')
                })
                .catch((error) => {
                    console.log("Maaf : ", error)
                })

        }else {
            Alert.alert('Maaf','Kotak judul dan catatan tidak boleh kosong')
        }
    }

    render() {
        return (
            <View style={styles.page}>
                <View style={styles.kotak}>
                    <InputTeks
                        placeholder='MASUKKAN JUDUL'
                        onChangeText={this.onChangeText}
                        value={this.state.judul}
                        namaState="judul"
                    />

                    <InputTeks
                        placeholder='MASUKKAN ISI CATATAN'
                        isTextArea={true}
                        onChangeText={this.onChangeText}
                        value={this.state.catatan}
                        namaState="catatan"
                    />

                    <View style={styles.kotakTbl}>
                        <TouchableOpacity style={styles.tambah} onPress={() => this.onSubmit()}>
                            <FontAwesomeIcon icon={faCheck} size={20} color={'white'} />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: 'white',
    },
    kotak: {
        padding: 20,
    },
    kotakTbl: {
        flex: 1,
        position: 'absolute',
        bottom: 40,
        right: 22,
    },
    tambah: {
        padding: 20,
        backgroundColor: 'orange',
        borderRadius: 30,
    },
})
