import { faEdit, faTimes } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'

const Tampil = ({id, catatanItem, navigation, removeData}) => {
    return (
        <TouchableOpacity style={styles.kotak} onPress={() => navigation.navigate('Detail', {id: id})}>
            <View style={styles.kotakCatatan}>
                <Text style={styles.judul}>{catatanItem.judul}</Text>
                <Text style={styles.catatan}>{catatanItem.catatan}</Text>
                <View style={styles.tombol}>
                    <TouchableOpacity onPress={() => navigation.navigate('EditCatatan', {id: id})}>
                        <FontAwesomeIcon icon={faEdit} size={30} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => removeData(id)}>
                        <FontAwesomeIcon icon={faTimes} size={30} style={styles.times} />
                    </TouchableOpacity>
                </View>
            </View>
        </TouchableOpacity>
    )
}

export default Tampil

const styles = StyleSheet.create({
    kotak: {
        paddingBottom: 15,
    },
    kotakCatatan: {
        backgroundColor: 'orange',
        padding: 15,
        borderRadius: 10,
        paddingBottom: 100,
    },
    judul: {
        fontSize: 14,
        fontWeight: 'bold',
    },
    tombol: {
        flexDirection: 'row',
        paddingTop: 20,
    },
    times: {
        paddingLeft: 50,
    }
})
