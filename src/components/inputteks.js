import React from 'react'
import { StyleSheet, View, TextInput } from 'react-native'

const InputTeks = ({
    placeholder, isTextArea, onChangeText, namaState, value
}) => {
    if (isTextArea) {
        return (
            <View>
                <TextInput style={styles.catatan}
                    multiline={true}
                    numberOfLines={31}
                    placeholder={placeholder}
                    placeholderTextColor='silver'
                    value={value}
                    onChangeText={(text) => onChangeText(namaState, text)}
                />
            </View>
        )
    }

    return (
        <View>
            <TextInput style={styles.judul}
                placeholder={placeholder}
                placeholderTextColor='silver'
                value={value}
                onChangeText={(text) => onChangeText(namaState, text)}
            />
        </View>
    )
}

export default InputTeks

const styles = StyleSheet.create({
    judul: {
        borderWidth: 1,
        padding: 10,
        color: 'black',
        marginBottom: 18,
    },
    catatan: {
        borderWidth: 1,
        padding: 10,
        color: 'black',
        marginBottom: 18,
        textAlignVertical: 'top',
    },
})
