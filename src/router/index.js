import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { Home, TulisCatatan, Detail, EditCatatan, Splash } from '../pages'

const Stack = createStackNavigator()

const Router = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen 
                name="Splash"
                component={Splash}
                options={{headerShown: false}}
            />
            <Stack.Screen 
                name="Home"
                component={Home}
                options={{headerShown: false}}
            />
            <Stack.Screen
                name="TulisCatatan"
                component={TulisCatatan}
                options={{
                    title: 'TAMBAH CATATAN',
                    headerTintColor: 'white',
                    headerTitleStyle: { fontWeight: 'bold' },
                    headerStyle: { backgroundColor: 'orange', height: 62 }
                }}
            />
            <Stack.Screen
                name="Detail"
                component={Detail}
                options={{
                    title: 'DETAIL CATATAN',
                    headerTintColor: 'white',
                    headerTitleStyle: { fontWeight: 'bold' },
                    headerStyle: { backgroundColor: 'orange', height: 62 }
                }}
            />
            <Stack.Screen
                name="EditCatatan"
                component={EditCatatan}
                options={{
                    title: 'EDIT CATATAN',
                    headerTintColor: 'white',
                    headerTitleStyle: { fontWeight: 'bold' },
                    headerStyle: { backgroundColor: 'orange', height: 62 }
                }}
            />
        </Stack.Navigator>
    )
}

export default Router
