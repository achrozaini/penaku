# Add project specific  rules here.
# By default, the flags in this file are appended to flags specified
# in /usr/local/Cellar/android-sdk/24.3.3/tools//-android.txt
# You can edit the include path and order by changing the Files
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/.html

# Add any project specific keep options here:
-keepattributes InnerClasses
